import 'package:flutter/material.dart';

Widget helloRectangle() {
  // This is usual func
  return Container(
    color: Colors.greenAccent,
  );
}

class HelloRectangle extends StatelessWidget {
  // This is usual class
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          color: Colors.greenAccent,
          height: 600.0,
          width: 600.0,
          child: Center(
            child: Text(
              'Hello',
              style: TextStyle(fontSize: 40.0),
            ),
          )),
    );
  }
}

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hello rectangle'),
        ),

        // body: Container( // Lets use func instead
        //   color: Colors.greenAccent,
        // ),
        // body: helloRectangle(), // Or even better - a CLASS
        body: HelloRectangle(),
      ),
    ),
  );
}
